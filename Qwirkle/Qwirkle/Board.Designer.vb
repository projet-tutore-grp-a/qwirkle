﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Board
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SwapBox = New System.Windows.Forms.PictureBox()
        Me.EndOfTurnButton = New System.Windows.Forms.Button()
        Me.SlotPawn6 = New System.Windows.Forms.PictureBox()
        Me.SlotPawn5 = New System.Windows.Forms.PictureBox()
        Me.SlotPawn4 = New System.Windows.Forms.PictureBox()
        Me.SlotPawn3 = New System.Windows.Forms.PictureBox()
        Me.SlotPawn2 = New System.Windows.Forms.PictureBox()
        Me.SlotPawn1 = New System.Windows.Forms.PictureBox()
        Me.PlayerPlayingLabel = New System.Windows.Forms.Label()
        Me.PlayingPlayerNameLabel = New System.Windows.Forms.Label()
        CType(Me.SwapBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlotPawn6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlotPawn5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlotPawn4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlotPawn3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlotPawn2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlotPawn1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(460, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(681, 681)
        Me.Panel1.TabIndex = 0
        '
        'SwapBox
        '
        Me.SwapBox.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SwapBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SwapBox.Image = Global.Qwirkle.My.Resources.Resources.Annuler
        Me.SwapBox.Location = New System.Drawing.Point(1123, 764)
        Me.SwapBox.Name = "SwapBox"
        Me.SwapBox.Size = New System.Drawing.Size(100, 100)
        Me.SwapBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SwapBox.TabIndex = 7
        Me.SwapBox.TabStop = False
        '
        'EndOfTurnButton
        '
        Me.EndOfTurnButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EndOfTurnButton.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Valider
        Me.EndOfTurnButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.EndOfTurnButton.Location = New System.Drawing.Point(1447, 764)
        Me.EndOfTurnButton.Name = "EndOfTurnButton"
        Me.EndOfTurnButton.Size = New System.Drawing.Size(100, 100)
        Me.EndOfTurnButton.TabIndex = 6
        Me.EndOfTurnButton.UseVisualStyleBackColor = True
        '
        'SlotPawn6
        '
        Me.SlotPawn6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SlotPawn6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SlotPawn6.Location = New System.Drawing.Point(907, 764)
        Me.SlotPawn6.Name = "SlotPawn6"
        Me.SlotPawn6.Size = New System.Drawing.Size(100, 100)
        Me.SlotPawn6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SlotPawn6.TabIndex = 5
        Me.SlotPawn6.TabStop = False
        '
        'SlotPawn5
        '
        Me.SlotPawn5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SlotPawn5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SlotPawn5.Location = New System.Drawing.Point(801, 764)
        Me.SlotPawn5.Name = "SlotPawn5"
        Me.SlotPawn5.Size = New System.Drawing.Size(100, 100)
        Me.SlotPawn5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SlotPawn5.TabIndex = 4
        Me.SlotPawn5.TabStop = False
        '
        'SlotPawn4
        '
        Me.SlotPawn4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SlotPawn4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SlotPawn4.Location = New System.Drawing.Point(695, 764)
        Me.SlotPawn4.Name = "SlotPawn4"
        Me.SlotPawn4.Size = New System.Drawing.Size(100, 100)
        Me.SlotPawn4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SlotPawn4.TabIndex = 3
        Me.SlotPawn4.TabStop = False
        '
        'SlotPawn3
        '
        Me.SlotPawn3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SlotPawn3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SlotPawn3.Location = New System.Drawing.Point(589, 764)
        Me.SlotPawn3.Name = "SlotPawn3"
        Me.SlotPawn3.Size = New System.Drawing.Size(100, 100)
        Me.SlotPawn3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SlotPawn3.TabIndex = 2
        Me.SlotPawn3.TabStop = False
        '
        'SlotPawn2
        '
        Me.SlotPawn2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SlotPawn2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SlotPawn2.Location = New System.Drawing.Point(483, 764)
        Me.SlotPawn2.Name = "SlotPawn2"
        Me.SlotPawn2.Size = New System.Drawing.Size(100, 100)
        Me.SlotPawn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SlotPawn2.TabIndex = 1
        Me.SlotPawn2.TabStop = False
        '
        'SlotPawn1
        '
        Me.SlotPawn1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.SlotPawn1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SlotPawn1.Location = New System.Drawing.Point(377, 764)
        Me.SlotPawn1.Name = "SlotPawn1"
        Me.SlotPawn1.Size = New System.Drawing.Size(100, 100)
        Me.SlotPawn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SlotPawn1.TabIndex = 0
        Me.SlotPawn1.TabStop = False
        '
        'PlayerPlayingLabel
        '
        Me.PlayerPlayingLabel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PlayerPlayingLabel.AutoSize = True
        Me.PlayerPlayingLabel.Font = New System.Drawing.Font("Impact", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlayerPlayingLabel.Location = New System.Drawing.Point(1369, 21)
        Me.PlayerPlayingLabel.Name = "PlayerPlayingLabel"
        Me.PlayerPlayingLabel.Size = New System.Drawing.Size(120, 21)
        Me.PlayerPlayingLabel.TabIndex = 8
        Me.PlayerPlayingLabel.Text = "C'est le tour de :"
        '
        'PlayingPlayerNameLabel
        '
        Me.PlayingPlayerNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PlayingPlayerNameLabel.AutoSize = True
        Me.PlayingPlayerNameLabel.Font = New System.Drawing.Font("Impact", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlayingPlayerNameLabel.Location = New System.Drawing.Point(1495, 21)
        Me.PlayingPlayerNameLabel.Name = "PlayingPlayerNameLabel"
        Me.PlayingPlayerNameLabel.Size = New System.Drawing.Size(13, 21)
        Me.PlayingPlayerNameLabel.TabIndex = 9
        Me.PlayingPlayerNameLabel.Text = " "
        '
        'Board
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1600, 900)
        Me.Controls.Add(Me.PlayingPlayerNameLabel)
        Me.Controls.Add(Me.PlayerPlayingLabel)
        Me.Controls.Add(Me.SwapBox)
        Me.Controls.Add(Me.EndOfTurnButton)
        Me.Controls.Add(Me.SlotPawn6)
        Me.Controls.Add(Me.SlotPawn5)
        Me.Controls.Add(Me.SlotPawn4)
        Me.Controls.Add(Me.SlotPawn3)
        Me.Controls.Add(Me.SlotPawn2)
        Me.Controls.Add(Me.SlotPawn1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "Board"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        CType(Me.SwapBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlotPawn6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlotPawn5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlotPawn4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlotPawn3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlotPawn2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlotPawn1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents SlotPawn1 As PictureBox
    Friend WithEvents SlotPawn2 As PictureBox
    Friend WithEvents SlotPawn3 As PictureBox
    Friend WithEvents SlotPawn4 As PictureBox
    Friend WithEvents SlotPawn5 As PictureBox
    Friend WithEvents SlotPawn6 As PictureBox
    Friend WithEvents EndOfTurnButton As Button
    Friend WithEvents SwapBox As PictureBox
    Friend WithEvents PlayerPlayingLabel As Label
    Friend WithEvents PlayingPlayerNameLabel As Label
End Class
