﻿Imports PawnClass
Imports PlayersClass
Imports System.Threading

Public Class Board
    Public Property ListPlayer As List(Of Player) 'Liste de joueurs définie en paramètres
    Dim GridX As Integer = 1 'Longueur du plateau affiché et limitation de la zone jouée
    Dim GridY As Integer = 1 'Hauteur du plateau affiché et limitation de la zone jouée
    Dim Grid(75, 75) As Pawn 'Tableau contenant les pions
    Dim PathPictureGrid(75, 75) As String 'Tableau de chemin de fichiers image

    Dim OriginPb As New PictureBox() 'PictureBox à la source du Drag


    Dim NbPlayer As Integer = 0 'Nombre de joueur
    Dim PlayerPlaying As Player 'Permet d'interagir sur le joueur dont c'est le tour
    Dim indexPlayerPlaying As Integer = 0 'Indice de liste du joueur en train de jouer

    Dim PbSlotImg As PictureBox 'Permet d'afficher les images des pions des joueurs dans les box prévues à cet effet

    Dim stock As New List(Of Pawn) 'Pioche


    Dim nbpawnswapped As Integer = 0 'Variable de nombre de pions échangés pendant un tour
    Dim nbpawnsplayedthisturn As Integer = 0
    Dim pawnsplayedthisturn As New List(Of Pawn)
    Dim CoordXpawnsplayedthisturn As New List(Of Integer)
    Dim CoordYpawnsplayedthisturn As New List(Of Integer)

    'CHARGEMENT DE LA FENETRE
    Private Sub Board_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Allow Drop sur les pictureBoxes d'emplacement de pions
        For Each HandPictureBoxes As Control In Me.Controls()
            If HandPictureBoxes.Name Like "SlotPawn*" Then
                HandPictureBoxes.AllowDrop = True
            End If
        Next

        'Initialisation de la pioche
        Dim Colors = New String() {"Rouge", "Bleu", "Orange", "Jaune", "Vert", "Violet"}
        Dim Symbols = New String() {"Trefle", "Etoile", "Carre", "Rond", "Losange", "Croix"}
        Dim NbTuile As Integer = 0
        For numColor As Byte = 0 To 5
            For numSymbol As Byte = 0 To 5
                For nPiece As Byte = 0 To 2
                    Dim APawn As New Pawn(Colors(numColor), Symbols(numSymbol))
                    NbTuile = NbTuile + 1
                    stock.Add(APawn)
                Next
            Next
        Next

        'Creation aléatoire de la main de départ de chaque joueur
        Dim rnd As New Random()
        For Each player As Player In ListPlayer
            NbPlayer = NbPlayer + 1
            Dim pos As New Integer
            For i As Integer = 0 To player.deck.Capacity
                pos = rnd.Next(0, stock.Count)
                player.deck.Add(stock.Item(pos))
                stock.RemoveAt(pos)
            Next
        Next

        'Code pour détermination du premier joueur
        Dim totalpotential As Integer = 0
        Dim index As Integer
        For counter As Integer = 0 To NbPlayer - 1
            Dim PotentialColor As Integer = 0
            Dim PotentialSymbol As Integer = 0
            For Each pawn As Pawn In ListPlayer(counter).deck
                For Each otherpawn As Pawn In ListPlayer(counter).deck
                    If (pawn.GetColor = otherpawn.GetColor And pawn.GetSymbol <> otherpawn.GetSymbol) Then
                        PotentialColor = PotentialColor + 1
                    End If
                    If (pawn.GetSymbol = otherpawn.GetSymbol And pawn.GetColor <> otherpawn.GetColor) Then
                        PotentialSymbol = PotentialSymbol + 1
                    End If
                    If (PotentialSymbol > totalpotential) Then
                        totalpotential = PotentialSymbol
                        index = counter
                    End If
                    If (PotentialColor > totalpotential) Then
                        totalpotential = PotentialColor
                        index = counter
                    End If
                Next
            Next
        Next
        indexPlayerPlaying = index
        PlayerPlaying = ListPlayer(indexPlayerPlaying)
        PlayingPlayerNameLabel.Text = PlayerPlaying.GetName 'Affichage du nom du premier joueur

        'Affichage de la main du premier joueur
        For nbpawn As Integer = 0 To 5
            PbSlotImg = Me.Controls("SlotPawn" & nbpawn + 1)
            Dim resources As Object = My.Resources.ResourceManager
            Dim pathimg As String = PlayerPlaying.deck.Item(nbpawn).GetPath()
            PbSlotImg.Image = My.Resources.ResourceManager.GetObject(pathimg)
            PbSlotImg.Tag = PlayerPlaying.deck.Item(nbpawn).GetPath()
        Next

        'Affichage initial des pictureBoxes du tableau
        InitTableau()

        'Création des GroupBoxes d'informations des joueurs
        Dim LastPGB As New GroupBox() 'Derniere GroupBox crée
        Dim NumPlayer As Byte = 1
        For Each APlayer As Player In ListPlayer
            Dim PlayerGroupBox As New GroupBox()
            Dim LabelScores As New Label()
            Dim LabelPoints As New Label()
            LabelScores.Text = "Score :"
            LabelScores.Width = 70
            LabelScores.Location = New Point(10, 30)
            LabelPoints.Name = "LabelPoints"
            LabelPoints.Text = APlayer.points
            LabelPoints.Width = 50
            LabelPoints.Location = New Point(10 + LabelScores.Size.Width + 20, 30)
            PlayerGroupBox.Name = "GroupBoxPlayer" & NumPlayer
            PlayerGroupBox.Text = APlayer.GetName()
            PlayerGroupBox.Width = 300
            PlayerGroupBox.Height = 70
            If NumPlayer = 1 Then
                PlayerGroupBox.Location = New Point(20, 20)
            Else
                PlayerGroupBox.Location = New Point(20, LastPGB.Location.Y + LastPGB.Size.Height + 20)
            End If
            PlayerGroupBox.Controls.Add(LabelScores)
            PlayerGroupBox.Controls.Add(LabelPoints)
            Me.Controls.Add(PlayerGroupBox)
            LastPGB = PlayerGroupBox
            NumPlayer = NumPlayer + 1
        Next


    End Sub

    Private Sub SlotPawn_MouseMove(sender As Object, e As MouseEventArgs) Handles SlotPawn1.MouseMove, SlotPawn2.MouseMove, SlotPawn3.MouseMove, SlotPawn4.MouseMove, SlotPawn5.MouseMove, SlotPawn6.MouseMove
        Dim EffetRealise As DragDropEffects
        If e.Button = MouseButtons.Left AndAlso sender.Image IsNot Nothing Then
            OriginPb = sender
            sender.AllowDrop = True
            EffetRealise = sender.DoDragDrop(sender.Image, DragDropEffects.Move)
        Else
            sender.AllowDrop = False
        End If

    End Sub

    Private Sub Pb_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub InitTableau()

        Dim LastPbCree As New PictureBox 'Derniere pb crée
        Dim nb As Byte
        Dim path As String = "Untitled" 'Chemin fichier image emplacement vide
        Panel1.AutoScroll = False
        For NbLine As Byte = 0 To (GridY - 1)
            For NbColumn As Byte = 0 To (GridX - 1)
                Dim pb As New PictureBox
                pb.SizeMode = PictureBoxSizeMode.StretchImage
                pb.Width = (Panel1.Size.Width) / (GridY)
                pb.Height = (Panel1.Size.Height) / (GridX)
                pb.BorderStyle = BorderStyle.FixedSingle
                pb.Name = "pb" & NbLine & ";" & NbColumn
                pb.AllowDrop = True
                pb.Image = My.Resources.ResourceManager.GetObject("Untitled")
                nb = Me.Panel1.Controls.Count
                AddHandler pb.DragEnter, AddressOf Pb_DragEnter
                AddHandler pb.DragDrop, AddressOf Pb_DragDrop
                If nb = 0 Then
                    pb.Location = New Point(0, 0)
                    LastPbCree = pb
                ElseIf NbColumn = 0 Then
                    pb.Location = New Point(0, LastPbCree.Location.Y + LastPbCree.Size.Height)
                    LastPbCree = pb
                Else
                    pb.Location = New Point(LastPbCree.Location.X + LastPbCree.Size.Width, LastPbCree.Location.Y)
                    LastPbCree = pb
                End If

                Grid(NbLine, NbColumn) = Nothing
                PathPictureGrid(NbLine, NbColumn) = path
                Me.Panel1.Controls.Add(pb)
            Next
        Next
    End Sub


    Private Sub RedimensionTableau()

        If (Panel1.Controls.Count <= 50) Then 'When Pictureboxes are still big enough in the panel
            Dim LastPbCree As New PictureBox
            Panel1.Controls.Clear()
            Dim nb As Byte
            Panel1.AutoScroll = False
            For NbLine As Byte = 0 To (GridY - 1)
                For NbColumn As Byte = 0 To (GridX - 1)
                    Dim pb As New PictureBox
                    'CHANGER TAILLE PANEL POUR QUE PB RESTENT DANS LE FORMAT DE 
                    pb.SizeMode = PictureBoxSizeMode.StretchImage
                    pb.Width = (Panel1.Size.Width) / (GridX)
                    pb.Height = (Panel1.Size.Height) / (GridY)
                    pb.BorderStyle = BorderStyle.FixedSingle
                    pb.Name = "pb" & NbLine & ";" & NbColumn
                    If (PathPictureGrid(NbLine, NbColumn) IsNot Nothing And PathPictureGrid(NbLine, NbColumn) <> "Untitled") Then
                        pb.Image = My.Resources.ResourceManager.GetObject(PathPictureGrid(NbLine, NbColumn))
                        pb.AllowDrop = False
                    Else
                        pb.Image = My.Resources.ResourceManager.GetObject("Untitled")
                        pb.AllowDrop = True
                    End If
                    nb = Me.Panel1.Controls.Count
                    AddHandler pb.DragEnter, AddressOf Pb_DragEnter
                    AddHandler pb.DragDrop, AddressOf Pb_DragDrop
                    If nb = 0 Then
                        pb.Location = New Point(0, 0)
                        LastPbCree = pb
                    ElseIf NbColumn = 0 Then
                        pb.Location = New Point(0, LastPbCree.Location.Y + LastPbCree.Size.Height)
                        LastPbCree = pb
                    Else
                        pb.Location = New Point(LastPbCree.Location.X + LastPbCree.Size.Width, LastPbCree.Location.Y)
                        LastPbCree = pb
                    End If
                    Me.Panel1.Controls.Add(pb)
                Next
            Next


        Else 'When Pictureboxes are getting too small
            Dim LastPbCree As New PictureBox
            Panel1.Controls.Clear()
            Dim nb As Integer
            Panel1.AutoScroll = True
            For NbLine As Integer = 0 To (GridY - 1)
                For NbColumn As Integer = 0 To (GridX - 1)
                    Dim pb As New PictureBox
                    pb.SizeMode = PictureBoxSizeMode.StretchImage
                    pb.Width = 90
                    pb.Height = 90
                    pb.BorderStyle = BorderStyle.FixedSingle
                    pb.Name = "pb" & NbLine & ";" & NbColumn

                    If (PathPictureGrid(NbLine, NbColumn) IsNot Nothing And PathPictureGrid(NbLine, NbColumn) <> "Untitled") Then
                        pb.Image = My.Resources.ResourceManager.GetObject(PathPictureGrid(NbLine, NbColumn))
                        pb.AllowDrop = False
                    Else
                        pb.Image = My.Resources.ResourceManager.GetObject("Untitled")
                        pb.AllowDrop = True
                    End If
                    nb = Me.Panel1.Controls.Count
                    AddHandler pb.DragEnter, AddressOf Pb_DragEnter
                    AddHandler pb.DragDrop, AddressOf Pb_DragDrop
                    If nb = 0 Then
                        pb.Location = New Point(0, 0)
                        LastPbCree = pb
                    ElseIf NbColumn = 0 Then
                        pb.Location = New Point(0, LastPbCree.Location.Y + LastPbCree.Size.Height)
                        LastPbCree = pb
                    Else
                        pb.Location = New Point(LastPbCree.Location.X + LastPbCree.Size.Width, LastPbCree.Location.Y)
                        LastPbCree = pb
                    End If
                    Try
                        Me.Panel1.Controls.Add(pb)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Next
            Next
        End If
    End Sub


    'Variables pour le drag_drop
    Dim Coord As String 'Chaine de caractères récupérant le nom de la pb dropped
    Dim CoordX As Integer 'Coordonnées X du pion posé sur le tableau
    Dim CoordY As Integer 'Coordonnées Y du pion posé sur le tableau

    Private Sub DecalageVersDroite()

        For i As Integer = GridY - 1 To 0 Step -1
            For j As Integer = GridX - 1 To 1 Step -1
                Grid(i, j) = Grid(i, j - 1)
                Grid(i, j - 1) = Nothing
                PathPictureGrid(i, j) = PathPictureGrid(i, j - 1)
                PathPictureGrid(i, j - 1) = "Untitled"
            Next
        Next

        For Each coordinateX As Integer In CoordXpawnsplayedthisturn
            coordinateX += 1
        Next
        CoordXpawnsplayedthisturn.RemoveAt(nbpawnsplayedthisturn)
        CoordXpawnsplayedthisturn.Add(CoordX)
        pawnsplayedthisturn.RemoveAt(nbpawnsplayedthisturn)
        pawnsplayedthisturn.Add(Grid(CoordY, CoordX))
    End Sub

    Private Sub DecalageVersBas()
        For i As Integer = GridY - 1 To 1 Step -1
            For j As Integer = GridX - 1 To 0 Step -1
                Grid(i, j) = Grid(i - 1, j)
                Grid(i - 1, j) = Nothing
                PathPictureGrid(i, j) = PathPictureGrid(i - 1, j)
                PathPictureGrid(i - 1, j) = "Untitled"
            Next
        Next

        For Each CoordinateY As Integer In CoordYpawnsplayedthisturn
            CoordinateY += 1
        Next

        CoordYpawnsplayedthisturn.RemoveAt(nbpawnsplayedthisturn)
        CoordYpawnsplayedthisturn.Add(CoordY)
        pawnsplayedthisturn.RemoveAt(nbpawnsplayedthisturn)
        pawnsplayedthisturn.Add(Grid(CoordY, CoordX))
    End Sub


    Dim PawnPlayedthisturn As Boolean = False

    Private Sub Pb_DragDrop(sender As Object, e As DragEventArgs)

        Dim dest As New PictureBox()

        'Algorithm getting coordinates(Line and Column on Board) of the dropped pawn
        Coord = ""
        For i As Integer = 0 To sender.Name.Length - 1
            If ("0" <= sender.Name(i) And sender.Name(i) <= "9") Then
                Coord += sender.Name(i)
            ElseIf sender.Name(i) = ";" Then
                CoordY = Coord
                Coord = ""
            End If
        Next
        CoordX = Coord

        'GETTING SYMBOL AND COLOR FROM THE DROPPED PAWN
        Dim path As String = OriginPb.Tag
        Dim Symbol As String = ""
        Dim Color As String
        Dim str As String = ""

        For i As Integer = 0 To path.Length - 1
            If path(i) = "-" Then
                Symbol = str
                str = ""
            Else
                str += path(i)
            End If
        Next
        Color = str

        'VERIFYING IF PAWN CAN BE DROPPED ON BOARD
        If (AllowDragDrop(New Pawn(Color, Symbol), CoordX, CoordY, Grid)) Then
            PawnPlayedthisturn = True
            sender.Image = e.Data.GetData(DataFormats.Bitmap)
            OriginPb.Image = Nothing
            PathPictureGrid(CoordY, CoordX) = OriginPb.Tag
            sender.Tag = OriginPb.Tag 'Tag Contains the filename of the image
            OriginPb.Tag = Nothing
            sender.AllowDrop = False
            Grid(CoordY, CoordX) = New Pawn(Color, Symbol)

            CoordXpawnsplayedthisturn.Add(CoordX)
            CoordYpawnsplayedthisturn.Add(CoordY)
            pawnsplayedthisturn.Add(Grid(CoordY, CoordX))

            'FOR THE FIRST EXPANSION
            If (GridX = 1 And GridY = 1) Then
                GridX = GridX + 1
                GridY = GridY + 1
            End If

            'DYNAMIC DISPLAY OF BOARD (ADD A COLUMN)
            Select Case CoordX
                Case GridX - 1
                    GridX = GridX + 1
                    CoordX += 1
                    RedimensionTableau()
                Case 0
                    GridX = GridX + 1
                    CoordX += 1
                    DecalageVersDroite()
                    RedimensionTableau()
            End Select

            'DYNAMIC DISPLAY OF BOARD (ADD A LINE)
            Select Case CoordY
                Case GridY - 1
                    GridY = GridY + 1
                    CoordY += 1
                    RedimensionTableau()
                Case 0
                    GridY = GridY + 1
                    CoordY += 1
                    DecalageVersBas()
                    RedimensionTableau()
            End Select
            Dim SizePlayerHand As Integer = PlayerPlaying.deck.Count - 1
            Dim HasBeenRemoved As Boolean = False
            Dim position As Integer = 0
            While (position <= SizePlayerHand)
                If (Color = PlayerPlaying.deck.Item(position).GetColor And Symbol = PlayerPlaying.deck.Item(position).GetSymbol And HasBeenRemoved = False) Then
                    PlayerPlaying.deck.RemoveAt(position)
                    SizePlayerHand = SizePlayerHand - 1
                    nbpawnsplayedthisturn += 1
                    HasBeenRemoved = True
                End If
                position = position + 1
            End While
            position = 0

        End If
    End Sub

    Function AllowChainDragDrop(xFirst As Integer, yFirst As Integer, xPut As Integer, yPut As Integer, tab As Pawn(,)) As Boolean
        Dim sameFirst, samePut, iStep, xAllow, yAllow As Integer
        xAllow = 0
        yAllow = 0
        If Not ((xFirst = xPut) Xor (yFirst = yPut)) Then
            Return False
        End If
        If (xFirst = xPut) Then
            sameFirst = yFirst
            samePut = yPut
            yAllow = 1
        ElseIf (yFirst = yPut) Then
            sameFirst = xFirst
            samePut = xPut
            xAllow = 1
        End If
        If (sameFirst - samePut > 0) Then
            iStep = -1
        ElseIf sameFirst - samePut < 0 Then
            iStep = 1
        End If
        Try
            For index As Integer = sameFirst To samePut Step iStep
                If (tab(index * xAllow, index * yAllow).GetSymbol() = "null") Then
                    Return False
                End If
            Next
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Dim DDFirst As Integer = 0
    Function AllowDragDrop(DDPawn As Pawn, y As Byte, x As Byte, tab As Pawn(,)) As Boolean
        Dim xAllow, yAllow As Integer
        Dim sameSymbol, sameColor As Boolean
        Dim combo As Byte
        Dim nbex As Byte = 0
        For card As Byte = 0 To 3
            xAllow = (card Mod 2) * (card - 2)
            yAllow = ((card + 1) Mod 2) * (card - 1)
            combo = 0
            Try
                While combo < 7
                    sameSymbol = (DDPawn.GetSymbol() = tab(x + xAllow * (combo + 1), y + yAllow * (combo + 1)).GetSymbol())
                    sameColor = (DDPawn.GetColor() = tab(x + xAllow * (combo + 1), y + yAllow * (combo + 1)).GetColor())
                    If (Not (sameSymbol Xor sameColor)) Then
                        Return False
                    End If
                    combo += 1
                End While
            Catch ex As Exception
                If combo = 0 Then
                    nbex += 1
                End If
            End Try
        Next
        If nbex = 4 And Not DDFirst = 0 Then
            Return False
        End If
        DDFirst += 1
        Return True
    End Function

    Dim PointFirst As Integer = 0
    Function PlayerPoints(pawnsPut As List(Of Pawn), Py As List(Of Integer), Px As List(Of Integer), tab As Pawn(,)) As Integer
        Dim PCounted As New List(Of Pawn)
        Dim PPuttedCounted As New List(Of Pawn)
        Dim xAllow, yAllow, cmb, shx, shy, PTotal As Integer
        Dim PPoints As Integer = 0
        Dim Index As Integer = 0
        Dim i As Integer = 0
        For Each PPutted As Pawn In pawnsPut
            PCounted.Add(PPutted)

            For card As Integer = 0 To 3
                PTotal = 0
                cmb = 0
                xAllow = (card Mod 2) * (card - 2)
                yAllow = ((card + 1) Mod 2) * (card - 1)
                shx = Px(Index) + xAllow * (cmb + 1)
                shy = Py(Index) + yAllow * (cmb + 1)
                Try

                    While (cmb < 7 And Not (tab(shx, shy).GetSymbol() = "null"))
                        If (Not PCounted.Contains(tab(shx, shy))) Then
                            PTotal += 1
                            PCounted.Add(tab(shx, shy))

                            PPoints += 1
                        Else

                        End If
                        cmb += 1
                        shx = Px(Index) + xAllow * (cmb + 1)
                        shy = Py(Index) + yAllow * (cmb + 1)

                    End While
                Catch ex As Exception

                End Try
                If (PTotal = 6) Then
                    PPoints += 6
                End If
                If (PTotal > 0) Then
                    PPoints += 1
                End If
            Next
            Index += 1

        Next
        PCounted.Clear()

        Return PPoints
    End Function

    Private Sub SwapBox_Click(sender As Object, e As EventArgs) Handles SwapBox.Click
        MessageBox.Show("Echange de tuiles! Vous ne pouvez plus placer de tuiles, placez les tuiles en bas à droite pour les échanger!")

        If (PawnPlayedthisturn = False) Then
            For NbLine As Byte = 0 To (GridY - 1)
                For NbColumn As Byte = 0 To (GridX - 1)
                    Dim pb As New PictureBox
                    pb = Panel1.Controls("pb" & NbLine & ";" & NbColumn)
                    pb.AllowDrop = False
                Next
            Next
            Dim swapbox As New PictureBox
            swapbox = Me.Controls("SwapBox")
            swapbox.AllowDrop = True
            swapbox.Image = My.Resources.ResourceManager.GetObject("Swap")
            AddHandler swapbox.DragEnter, AddressOf Pb_DragEnter
            AddHandler swapbox.DragDrop, AddressOf SwapBox_DragDrop
        End If
    End Sub


    Private Sub SwapBox_DragDrop(sender As Object, e As DragEventArgs) Handles SwapBox.DragDrop
        Dim hasaddedapawn As Boolean = False


        If (PawnPlayedthisturn = False) Then
            SwapBox.Tag = OriginPb.Tag
            OriginPb.Tag = "Untitled"
            OriginPb.Image = Nothing
            Dim path As String
            Dim Color As String
            Dim Symbol As String = ""
            Dim Str As String = ""
            path = SwapBox.Tag
            For i As Integer = 0 To path.Length - 1
                If path(i) = "-" Then
                    Symbol = Str
                    Str = ""
                Else
                    Str += path(i)
                End If
            Next
            Color = Str
            If (String.Equals(Color, "Untitled")) Then
            Else
                Dim APawn As New Pawn(Color, Symbol)
                stock.Add(APawn)
                hasaddedapawn = True

                Dim SizePlayerHand As Integer = PlayerPlaying.deck.Count - 1
                Dim HasBeenRemoved As Boolean = False
                Dim position As Integer = 0
                While (position <= SizePlayerHand)
                    If (Color = PlayerPlaying.deck.Item(position).GetColor And Symbol = PlayerPlaying.deck.Item(position).GetSymbol And HasBeenRemoved = False) Then
                        PlayerPlaying.deck.RemoveAt(position)
                        SizePlayerHand = SizePlayerHand - 1
                        nbpawnswapped = nbpawnswapped + 1
                        HasBeenRemoved = True

                    End If
                    position = position + 1
                End While
                position = 0
            End If
        End If
    End Sub

    Private Sub EndOfTurnButton_Click(sender As Object, e As EventArgs) Handles EndOfTurnButton.Click

        Dim Pointsthisturn As Integer

        Me.Hide()
        MessageBox.Show("Fin du tour, Appuyez sur OK quand le joueur suivant est prêt!")

        Pointsthisturn = PlayerPoints(pawnsplayedthisturn, CoordXpawnsplayedthisturn, CoordYpawnsplayedthisturn, Grid)

        RefillHand(nbpawnsplayedthisturn)
        RefillHand(nbpawnswapped)


        PawnPlayedthisturn = False

        For NbLine As Byte = 0 To (GridY - 1)
            For NbColumn As Byte = 0 To (GridX - 1)
                Dim pb As New PictureBox
                pb = Panel1.Controls("pb" & NbLine & ";" & NbColumn)
                pb.AllowDrop = True
                SwapBox.Image = My.Resources.ResourceManager.GetObject("Annuler")
                SwapBox.AllowDrop = False
            Next
        Next

        Dim PlayerGroupBox As New GroupBox()
        Dim LabelPoints As New Label()
        PlayerGroupBox = Me.Controls("GroupBoxPlayer" & indexPlayerPlaying + 1)
        LabelPoints = PlayerGroupBox.Controls("LabelPoints")
        PlayerPlaying.points = Pointsthisturn + PlayerPlaying.points
        LabelPoints.Text = PlayerPlaying.points

        indexPlayerPlaying = indexPlayerPlaying + 1
        If (indexPlayerPlaying = NbPlayer) Then
            indexPlayerPlaying = 0
        End If



        PlayerPlaying = ListPlayer(indexPlayerPlaying)



        For nbpawn As Integer = 0 To 5
            PbSlotImg = Me.Controls("SlotPawn" & nbpawn + 1)
            Dim resources As Object = My.Resources.ResourceManager
            Dim pathimg As String = PlayerPlaying.deck.Item(nbpawn).GetPath()
            PbSlotImg.Image = My.Resources.ResourceManager.GetObject(pathimg)
            PbSlotImg.Tag = PlayerPlaying.deck.Item(nbpawn).GetPath()
            PbSlotImg.AllowDrop = True
            If (PbSlotImg.Tag = "Untitled") Then
                PbSlotImg.AllowDrop = False
            End If
        Next

        CoordXpawnsplayedthisturn.Clear()
        CoordYpawnsplayedthisturn.Clear()
        pawnsplayedthisturn.Clear()

        PlayingPlayerNameLabel.Text = PlayerPlaying.GetName()
        nbpawnswapped = 0
        nbpawnsplayedthisturn = 0
        Me.Show()
    End Sub

    Private Sub RefillHand(NumberToReplace As Integer)
        For Counter As Integer = 0 To NumberToReplace - 1
            Dim rnd As New Random()
            Dim pos As New Integer
            pos = rnd.Next(0, stock.Count)
            PlayerPlaying.deck.Add(stock.Item(pos))
            stock.RemoveAt(pos)
        Next
    End Sub
End Class