﻿Imports PlayersClass

Public Class Parametres
    Dim ListPlayer As New List(Of Player)

    Public Property numberofgamesplayed As Integer = TitleScreen.numberofgamesplayed

    Private Sub Retour_Click(sender As Object, e As EventArgs) Handles Retour.Click
        TitleScreen.Show()
        Me.Close()
    End Sub

    Private Sub PlayerTrackBar_ValueChanged(sender As Object, e As EventArgs) Handles PlayerTrackBar.ValueChanged
        Select Case PlayerTrackBar.Value
            Case 3
                PlayersGroupBox.Enabled = True
                Player3Name.Enabled = True
                Player4Name.Text = Nothing
                Player4Name.Enabled = False
                LblP3.Enabled = True
                LblP4.Enabled = False
            Case 4
                PlayersGroupBox.Enabled = True
                Player4Name.Enabled = True
                LblP4.Enabled = True
            Case Else
                PlayersGroupBox.Enabled = False
                Player3Name.Text = Nothing
                Player4Name.Text = Nothing
        End Select
    End Sub

    Private Sub StartPlaying_Click(sender As Object, e As EventArgs) Handles StartPlaying.Click
        Dim NbPlayers As Integer = PlayerTrackBar.Value
        Dim NamePlayer As String = ""

        numberofgamesplayed += 1

        For i As Integer = 1 To NbPlayers
            Dim Tbox As TextBox

            If i = 2 Or i = 1 Then
                Tbox = Me.Controls("Player" & i & "Name")
                If (Tbox.Text = "") Then
                    NamePlayer = "Joueur " & i
                Else
                    NamePlayer = Tbox.Text
                End If
            End If

            If i >= 3 Then
                Tbox = Me.PlayersGroupBox.Controls("Player" & i & "Name")
                If (Tbox.Text = "") Then
                    NamePlayer = "Joueur " & i
                Else
                    NamePlayer = Tbox.Text
                End If

            End If
            Dim aPlayer As Player = New Player(NamePlayer)
            ListPlayer.Add(aPlayer)
        Next
        Board.ListPlayer = Me.ListPlayer
        Me.Close()
        Board.Show()
    End Sub
End Class