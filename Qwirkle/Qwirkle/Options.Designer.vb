﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Options
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ResolutionBox = New System.Windows.Forms.ComboBox()
        Me.CreditsButton = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button1.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.Button1.Location = New System.Drawing.Point(1389, 826)
        Me.Button1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(199, 63)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Retour"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ResolutionBox
        '
        Me.ResolutionBox.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ResolutionBox.Font = New System.Drawing.Font("Impact", 20.0!)
        Me.ResolutionBox.FormattingEnabled = True
        Me.ResolutionBox.ItemHeight = 41
        Me.ResolutionBox.Location = New System.Drawing.Point(954, 437)
        Me.ResolutionBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ResolutionBox.Name = "ResolutionBox"
        Me.ResolutionBox.Size = New System.Drawing.Size(199, 49)
        Me.ResolutionBox.TabIndex = 1
        Me.ResolutionBox.Text = "RESOLUTION"
        '
        'CreditsButton
        '
        Me.CreditsButton.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CreditsButton.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.CreditsButton.Location = New System.Drawing.Point(448, 414)
        Me.CreditsButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.CreditsButton.Name = "CreditsButton"
        Me.CreditsButton.Size = New System.Drawing.Size(199, 63)
        Me.CreditsButton.TabIndex = 8
        Me.CreditsButton.Text = "CREDITS"
        Me.CreditsButton.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.Image = Global.Qwirkle.My.Resources.Resources.Logo
        Me.PictureBox1.Location = New System.Drawing.Point(374, 35)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(853, 250)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'Options
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1600, 900)
        Me.Controls.Add(Me.CreditsButton)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.ResolutionBox)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "Options"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents ResolutionBox As ComboBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents CreditsButton As Button
End Class
