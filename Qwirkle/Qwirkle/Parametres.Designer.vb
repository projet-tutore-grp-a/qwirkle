﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Parametres
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PlayerTrackBar = New System.Windows.Forms.TrackBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LblP1 = New System.Windows.Forms.Label()
        Me.LblP2 = New System.Windows.Forms.Label()
        Me.Player1Name = New System.Windows.Forms.TextBox()
        Me.Player2Name = New System.Windows.Forms.TextBox()
        Me.StartPlaying = New System.Windows.Forms.Button()
        Me.PlayersGroupBox = New System.Windows.Forms.GroupBox()
        Me.Player4Name = New System.Windows.Forms.TextBox()
        Me.LblP4 = New System.Windows.Forms.Label()
        Me.Player3Name = New System.Windows.Forms.TextBox()
        Me.LblP3 = New System.Windows.Forms.Label()
        Me.Retour = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlayerTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PlayersGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.Image = Global.Qwirkle.My.Resources.Resources.Logo
        Me.PictureBox1.Location = New System.Drawing.Point(374, 35)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(853, 250)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'PlayerTrackBar
        '
        Me.PlayerTrackBar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PlayerTrackBar.LargeChange = 1
        Me.PlayerTrackBar.Location = New System.Drawing.Point(793, 294)
        Me.PlayerTrackBar.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PlayerTrackBar.Maximum = 4
        Me.PlayerTrackBar.Minimum = 2
        Me.PlayerTrackBar.Name = "PlayerTrackBar"
        Me.PlayerTrackBar.Size = New System.Drawing.Size(344, 56)
        Me.PlayerTrackBar.TabIndex = 9
        Me.PlayerTrackBar.Value = 2
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.Font = New System.Drawing.Font("Impact", 20.0!)
        Me.Label1.Location = New System.Drawing.Point(464, 294)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(323, 48)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Nombre de joueurs :"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Impact", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(803, 329)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 21)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "2"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Impact", 10.0!)
        Me.Label3.Location = New System.Drawing.Point(958, 329)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 21)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "3"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Impact", 10.0!)
        Me.Label4.Location = New System.Drawing.Point(1109, 329)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(18, 21)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "4"
        '
        'LblP1
        '
        Me.LblP1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LblP1.Font = New System.Drawing.Font("Impact", 20.0!)
        Me.LblP1.Location = New System.Drawing.Point(492, 435)
        Me.LblP1.Name = "LblP1"
        Me.LblP1.Size = New System.Drawing.Size(295, 48)
        Me.LblP1.TabIndex = 15
        Me.LblP1.Text = "Nom du joueur 1 :"
        Me.LblP1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblP2
        '
        Me.LblP2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblP2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LblP2.Font = New System.Drawing.Font("Impact", 20.0!)
        Me.LblP2.Location = New System.Drawing.Point(492, 499)
        Me.LblP2.Name = "LblP2"
        Me.LblP2.Size = New System.Drawing.Size(295, 48)
        Me.LblP2.TabIndex = 19
        Me.LblP2.Text = "Nom du joueur 2 :"
        Me.LblP2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Player1Name
        '
        Me.Player1Name.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Player1Name.Font = New System.Drawing.Font("Impact", 19.0!)
        Me.Player1Name.Location = New System.Drawing.Point(807, 435)
        Me.Player1Name.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Player1Name.MaxLength = 20
        Me.Player1Name.Name = "Player1Name"
        Me.Player1Name.Size = New System.Drawing.Size(321, 46)
        Me.Player1Name.TabIndex = 22
        '
        'Player2Name
        '
        Me.Player2Name.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Player2Name.Font = New System.Drawing.Font("Impact", 19.0!)
        Me.Player2Name.Location = New System.Drawing.Point(807, 499)
        Me.Player2Name.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Player2Name.MaxLength = 20
        Me.Player2Name.Name = "Player2Name"
        Me.Player2Name.Size = New System.Drawing.Size(321, 46)
        Me.Player2Name.TabIndex = 23
        '
        'StartPlaying
        '
        Me.StartPlaying.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.StartPlaying.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.StartPlaying.Location = New System.Drawing.Point(491, 753)
        Me.StartPlaying.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.StartPlaying.Name = "StartPlaying"
        Me.StartPlaying.Size = New System.Drawing.Size(636, 59)
        Me.StartPlaying.TabIndex = 26
        Me.StartPlaying.Text = "COMMENCER A JOUER"
        Me.StartPlaying.UseVisualStyleBackColor = True
        '
        'PlayersGroupBox
        '
        Me.PlayersGroupBox.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PlayersGroupBox.Controls.Add(Me.Player4Name)
        Me.PlayersGroupBox.Controls.Add(Me.LblP4)
        Me.PlayersGroupBox.Controls.Add(Me.Player3Name)
        Me.PlayersGroupBox.Controls.Add(Me.LblP3)
        Me.PlayersGroupBox.Enabled = False
        Me.PlayersGroupBox.Font = New System.Drawing.Font("Impact", 9.0!)
        Me.PlayersGroupBox.Location = New System.Drawing.Point(481, 560)
        Me.PlayersGroupBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PlayersGroupBox.Name = "PlayersGroupBox"
        Me.PlayersGroupBox.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PlayersGroupBox.Size = New System.Drawing.Size(656, 148)
        Me.PlayersGroupBox.TabIndex = 27
        Me.PlayersGroupBox.TabStop = False
        Me.PlayersGroupBox.Text = "Joueurs supplémentaires"
        '
        'Player4Name
        '
        Me.Player4Name.Enabled = False
        Me.Player4Name.Font = New System.Drawing.Font("Impact", 19.0!)
        Me.Player4Name.Location = New System.Drawing.Point(325, 85)
        Me.Player4Name.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Player4Name.MaxLength = 20
        Me.Player4Name.Name = "Player4Name"
        Me.Player4Name.Size = New System.Drawing.Size(321, 46)
        Me.Player4Name.TabIndex = 28
        '
        'LblP4
        '
        Me.LblP4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LblP4.Font = New System.Drawing.Font("Impact", 20.0!)
        Me.LblP4.Location = New System.Drawing.Point(11, 82)
        Me.LblP4.Name = "LblP4"
        Me.LblP4.Size = New System.Drawing.Size(295, 48)
        Me.LblP4.TabIndex = 27
        Me.LblP4.Text = "Nom du joueur 4 :"
        Me.LblP4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Player3Name
        '
        Me.Player3Name.Enabled = False
        Me.Player3Name.Font = New System.Drawing.Font("Impact", 19.0!)
        Me.Player3Name.Location = New System.Drawing.Point(325, 20)
        Me.Player3Name.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Player3Name.MaxLength = 20
        Me.Player3Name.Name = "Player3Name"
        Me.Player3Name.Size = New System.Drawing.Size(321, 46)
        Me.Player3Name.TabIndex = 25
        '
        'LblP3
        '
        Me.LblP3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LblP3.Font = New System.Drawing.Font("Impact", 20.0!)
        Me.LblP3.Location = New System.Drawing.Point(11, 18)
        Me.LblP3.Name = "LblP3"
        Me.LblP3.Size = New System.Drawing.Size(295, 48)
        Me.LblP3.TabIndex = 21
        Me.LblP3.Text = "Nom du joueur 3 :"
        Me.LblP3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Retour
        '
        Me.Retour.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Retour.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.Retour.Location = New System.Drawing.Point(1389, 826)
        Me.Retour.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Retour.Name = "Retour"
        Me.Retour.Size = New System.Drawing.Size(199, 63)
        Me.Retour.TabIndex = 8
        Me.Retour.Text = "Retour"
        Me.Retour.UseVisualStyleBackColor = True
        '
        'Parametres
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1600, 900)
        Me.Controls.Add(Me.PlayersGroupBox)
        Me.Controls.Add(Me.StartPlaying)
        Me.Controls.Add(Me.Player2Name)
        Me.Controls.Add(Me.Player1Name)
        Me.Controls.Add(Me.LblP2)
        Me.Controls.Add(Me.LblP1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PlayerTrackBar)
        Me.Controls.Add(Me.Retour)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "Parametres"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlayerTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PlayersGroupBox.ResumeLayout(False)
        Me.PlayersGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PlayerTrackBar As TrackBar
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents LblP1 As Label
    Friend WithEvents LblP2 As Label
    Friend WithEvents Player1Name As TextBox
    Friend WithEvents Player2Name As TextBox
    Friend WithEvents StartPlaying As Button
    Friend WithEvents PlayersGroupBox As GroupBox
    Friend WithEvents Player3Name As TextBox
    Friend WithEvents LblP3 As Label
    Friend WithEvents Player4Name As TextBox
    Friend WithEvents LblP4 As Label
    Friend WithEvents Retour As Button
End Class
