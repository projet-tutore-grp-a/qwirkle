﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TitleScreen
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PLAY = New System.Windows.Forms.Button()
        Me.OPTIONbutton = New System.Windows.Forms.Button()
        Me.REGLES = New System.Windows.Forms.Button()
        Me.QUITTER = New System.Windows.Forms.Button()
        Me.nbpartiesjouees = New System.Windows.Forms.Label()
        Me.GameCounter = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PLAY
        '
        Me.PLAY.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PLAY.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.PLAY.Location = New System.Drawing.Point(701, 396)
        Me.PLAY.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PLAY.Name = "PLAY"
        Me.PLAY.Size = New System.Drawing.Size(199, 63)
        Me.PLAY.TabIndex = 0
        Me.PLAY.Text = "JOUER"
        Me.PLAY.UseVisualStyleBackColor = True
        '
        'OPTIONbutton
        '
        Me.OPTIONbutton.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OPTIONbutton.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.OPTIONbutton.Location = New System.Drawing.Point(701, 534)
        Me.OPTIONbutton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.OPTIONbutton.Name = "OPTIONbutton"
        Me.OPTIONbutton.Size = New System.Drawing.Size(199, 63)
        Me.OPTIONbutton.TabIndex = 1
        Me.OPTIONbutton.Text = "OPTIONS"
        Me.OPTIONbutton.UseVisualStyleBackColor = True
        '
        'REGLES
        '
        Me.REGLES.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.REGLES.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.REGLES.Location = New System.Drawing.Point(701, 672)
        Me.REGLES.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.REGLES.Name = "REGLES"
        Me.REGLES.Size = New System.Drawing.Size(199, 63)
        Me.REGLES.TabIndex = 2
        Me.REGLES.Text = "REGLES"
        Me.REGLES.UseVisualStyleBackColor = True
        '
        'QUITTER
        '
        Me.QUITTER.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.QUITTER.Font = New System.Drawing.Font("Impact", 16.0!)
        Me.QUITTER.Location = New System.Drawing.Point(1389, 826)
        Me.QUITTER.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.QUITTER.Name = "QUITTER"
        Me.QUITTER.Size = New System.Drawing.Size(199, 63)
        Me.QUITTER.TabIndex = 3
        Me.QUITTER.Text = "QUITTER"
        Me.QUITTER.UseVisualStyleBackColor = True
        '
        'nbpartiesjouees
        '
        Me.nbpartiesjouees.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.nbpartiesjouees.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbpartiesjouees.Font = New System.Drawing.Font("Impact", 12.0!)
        Me.nbpartiesjouees.Location = New System.Drawing.Point(12, 826)
        Me.nbpartiesjouees.Name = "nbpartiesjouees"
        Me.nbpartiesjouees.Size = New System.Drawing.Size(245, 48)
        Me.nbpartiesjouees.TabIndex = 4
        Me.nbpartiesjouees.Text = "NOMBRE DE PARTIES JOUEES :"
        Me.nbpartiesjouees.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GameCounter
        '
        Me.GameCounter.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GameCounter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GameCounter.Font = New System.Drawing.Font("Impact", 12.0!)
        Me.GameCounter.Location = New System.Drawing.Point(263, 826)
        Me.GameCounter.Name = "GameCounter"
        Me.GameCounter.Size = New System.Drawing.Size(62, 48)
        Me.GameCounter.TabIndex = 5
        Me.GameCounter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.Image = Global.Qwirkle.My.Resources.Resources.Logo
        Me.PictureBox1.Location = New System.Drawing.Point(374, 35)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(853, 250)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'TitleScreen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1600, 900)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GameCounter)
        Me.Controls.Add(Me.nbpartiesjouees)
        Me.Controls.Add(Me.QUITTER)
        Me.Controls.Add(Me.REGLES)
        Me.Controls.Add(Me.OPTIONbutton)
        Me.Controls.Add(Me.PLAY)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "TitleScreen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PLAY As Button
    Friend WithEvents OPTIONbutton As Button
    Friend WithEvents REGLES As Button
    Friend WithEvents QUITTER As Button
    Friend WithEvents nbpartiesjouees As Label
    Friend WithEvents GameCounter As Label
    Friend WithEvents PictureBox1 As PictureBox
End Class
