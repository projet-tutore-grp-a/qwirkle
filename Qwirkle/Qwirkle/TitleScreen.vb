﻿Public Class TitleScreen

    Public numberofgamesplayed As Integer = 0

    Private Sub QUITTER_Click(sender As Object, e As EventArgs) Handles QUITTER.Click
        Me.Close()
    End Sub

    Private Sub REGLES_Click(sender As Object, e As EventArgs) Handles REGLES.Click
        Process.Start("www.ultraqwirkle.com/game-rules.php")
    End Sub

    Private Sub OPTIONS_Click(sender As Object, e As EventArgs) Handles OPTIONbutton.Click
        Options.Show()
        Me.Hide()
    End Sub

    Private Sub PLAY_Click(sender As Object, e As EventArgs) Handles PLAY.Click
        Parametres.Show()
        Me.Hide()
    End Sub

    Private Sub TitleScreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GameCounter.Text = numberofgamesplayed
    End Sub
End Class
