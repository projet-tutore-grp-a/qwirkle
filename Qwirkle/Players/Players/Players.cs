﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PawnClass;

namespace PlayersClass
{
    public class Player
    {
        private string name;
        public List<Pawn> deck;
        public int points;

        public Player(string name)
        {
            deck = new List<Pawn>(5);
            this.name=name;
            points = 0;
        }

        public string GetName()
        {
            return name;
        }

    }
}
