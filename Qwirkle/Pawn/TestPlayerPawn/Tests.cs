﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PawnClass;
using PlayersClass;

namespace TestPlayerPawn
{
    [TestClass]

    public class PawnTest
    {
        [TestMethod]
        public void TestColor()
        {
            //Pawn test = new Pawn("Orange", "Croix");
            Assert.AreEqual("Orange", test.GetColor());
        }

        [TestMethod]
        public void TestSymbol()
        {
            Pawn test = new Pawn("Orange", "Croix");
            Assert.AreEqual("Croix", test.GetSymbol());

        }

        public void TestPath()
        {
            Pawn test = new Pawn("Orange", "Croix");
            Assert.AreEqual("Orange-Croix", test.GetPath());

            Pawn test = new Pawn("null", "null");
            Assert.AreEqual("Untitled", test.GetPath());

        }



    }
    [TestClass]
    public class PlayerTest
    {
        [TestMethod]
        public void TestName()
        {
            Player test = new Player("Flavien");
            Assert.AreEqual("Flavien", test.GetName());
        }

        [TestMethod]
        public void TestPoints()
        {
            Player test = new Player("Quentin");
            Assert.AreEqual(0, test.points);
        }

    }
}
