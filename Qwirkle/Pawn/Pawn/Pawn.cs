﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawnClass
{
    public class Pawn
    {
        private string color;
        private string symbol;
        private string path;

        public Pawn(string color, string symbol)
        {
            this.color = color;
            this.symbol = symbol;
            path = string.Format("{0}-{1}", symbol, color);
            if (string.Equals(path,"null-null"))
            {
                path = "Untitled";
            }
        }

        public string GetColor()
        {
            return color;
        }

        public string GetSymbol()
        {
            return symbol;
        }

        public string GetPath()
        {
            return path;
        }
    }
}
